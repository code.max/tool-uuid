<?php
namespace code.max\tool;

abstract class UUID 
{

	/**
	 *
	 * @var string
	 */
	const pattern_type = '^[a-z0-9]{32}$';
	
	/**
	 *
	 * @param string $uuid
	 * @return bool
	 */
	static public function validate( string $uuid ) : bool 
	{ 
		return preg_match( '/'.UUID::pattern_type.'/', $uuid ); 
	}
	
	/**
	 *
	 * @return string
	 */
	static public function generate() : string 
	{
		$key = random_bytes( 24 ).microtime();
		$entropy = memory_get_usage() * pi();
		return md5( $key.$entropy );
	}
	
}
